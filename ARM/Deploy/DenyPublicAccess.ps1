﻿# Get current setting for Public Network Access
# az sql server show -n server-security-test --resource-group rg-cloudsecurity-test --query "publicNetworkAccess"

# Update setting for Public Network Access

az sql server update -n server-cloudsecurity-test --resource-group rg-cloudsecurity-test --set publicNetworkAccess="Disabled"