﻿
function Create-KeyVault-IfNotExists
{
    [CmdLetBinding()]
    param(
        [Parameter()]
        [string]$Name,
        [Parameter()]
        [string]$ResourceGroupName,
        [Parameter()]
        [string]$Location,
        [Parameter()]
        [string]$Sku = "Standard"
     )
    
     Get-AzResourceGroup -Name $ResourceGroupName -Location $Location -ErrorVariable notPresent -ErrorAction SilentlyContinue
    
     if ($notPresent)
     {
         Write-Host "Setting up resource group ..."
         New-AzResourceGroup -Name $ResourceGroupName -Location $Location
     }
     else
     {
         Write-Host "Alredy set up resource group -> Not doing anything"
     }
    
    $keyVaultResourceInfo=Get-AzResource -ResourceName $Name -ResourceGroupName $ResourceGroupName
    $context = Get-AzContext
    $principal = Get-AzADServicePrincipal -ApplicationId $context.Account.Id

    if($keyVaultResourceInfo) {
        Write-Host "Already set up vault. Setting policies"
        Write-Host $principal.Id

        Write-Host "setting up policy"
        Set-AzKeyVaultAccessPolicy -VaultName $Name -PermissionsToCertificates update,list,create,get -ObjectID $principal.Id
        Write-Host "Policy set up"

    }
    else {
    Write-Host "setting up keyvault"
        New-AzKeyVault `
        -Name $Name `
        -Location $Location `
        -EnabledForDeployment `
        -EnabledForTemplateDeployment `
        -EnabledForDiskEncryption `
        -ResourceGroupName $ResourceGroupName `
        -Sku $Sku
        Write-Host "Keyvault set up"

        Write-Host "setting up policy"
        Set-AzKeyVaultAccessPolicy -VaultName $Name -PermissionsToCertificates update,list,create,get -ObjectID $principal.Id
        Write-Host "Policy set up"
    }
}

function Create-SelfSignedCertificate-IfNotExists
{
    [CmdLetBinding()]
    param(
    [Parameter()]
    [string] $VaultName,
    [Parameter()]
    [string] $CertName,
    [Parameter()]
    [string] $SubjectName)

    Write-Host "Getting certificate"
    $cert = Get-AzKeyVaultCertificate -VaultName $VaultName -Name $CertName

    if($cert) {
        Write-Host "Cert already exists"
	}
    else {
        Write-Host "Creating cert"
        $Policy = New-AzKeyVaultCertificatePolicy -SecretContentType "application/x-pkcs12" -SubjectName $SubjectName -IssuerName "Self" -ValidityInMonths 6 -ReuseKeyOnRenewal
        Add-AzKeyVaultCertificate -VaultName $VaultName -Name $CertName -CertificatePolicy $Policy

        Write-Host "Cert created"

	}
}

Create-KeyVault-IfNotExists -Name 'kv-cloudsecurity-test' -ResourceGroupName 'rg-cloudsecurity-test' -Location 'west europe'
Create-SelfSignedCertificate-IfNotExists -VaultName 'kv-cloudsecurity-test' -CertName 'httpscert' -SubjectName 'CN=azurewebsites.net'