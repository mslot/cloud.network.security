﻿function Remove-RestrictionRule-From-ScmSite {
	[CmdletBinding()]
	param(
		[string] 
		$WebAppName,
		[string] 
		$ResourceGroupName,
		[string]
		$SlotName = "production",
		[string]
		$RuleName
	)
	try {
		$webappInfo = Get-AzResource -ResourceName $WebAppName -ResourceGroupName $ResourceGroupName

		if($webappInfo){
			Remove-AzWebAppAccessRestrictionRule -ResourceGroupName $ResourceGroupName -WebAppName $WebAppName -SlotName $SlotName -Name $RuleName -TargetScmSite
		}
	} catch {
		Write-Host "Couldn't retrieve or remove restriction on webapp"
	}
}

Remove-RestrictionRule-From-ScmSite -WebAppName 'proxycloudsecuritywebapp' -ResourceGroupName 'rg-cloudsecurity-test' -RuleName 'GatewayRule' -SlotName 'stagingslot'