﻿
# Adds a given VNET subnet to SCM site - this ignores missing service point
function Add-Subnet-To-ScmSite {
	[CmdletBinding()]
	param(
		[string] 
		$WebAppName,
		[string] 
		$ResourceGroupName,
		[string]
		$SlotName = "production",
		[string]
		$SubnetName = "GatewaySubnet",
		[string]
		$VNetName,
		[string]
		$RuleName,
		[int]
		$Priority = 1
	)
	$vnetInfo = Get-AzResource -ResourceName $VNetName -ResourceGroupName $ResourceGroupName

	if($vnetInfo) {
		$vnet = Get-AzVirtualNetwork -Name $VnetName
		$gatewaySubnet = $vnet.Subnets.Where({$_.Name-eq $SubnetName})
		$gatewayId = $gatewaySubnet.Id
		Add-AzWebAppAccessRestrictionRule -ResourceGroupName $ResourceGroupName -WebAppName $WebAppName -SlotName $SlotName -Name $RuleName -Priority $Priority -TargetScmSite -Action Allow -SubnetId $gatewayId -IgnoreMissingServiceEndpoint
	}
}

Add-Subnet-To-ScmSite -WebAppName 'proxycloudsecuritywebapp' -ResourceGroupName 'rg-cloudsecurity-test' -VnetName 'vnet-cloudsecurity-test' -RuleName 'GatewayRule' -SlotName 'stagingslot'